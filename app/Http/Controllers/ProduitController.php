<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    public function index() {
		return view("catalogue", ["articles" => Produit::all()]);
    }

    public function description(Produit $produit) {
		return view("description", ["articles" => $produit]);
    }
}

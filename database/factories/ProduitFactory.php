<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Produit>
 */
class ProduitFactory extends Factory
{
	/**
	 * Define the model's default state.
	 *
	 * @return array<string, mixed>
	 */
	public function definition()
	{
		return [
			//
			"id" => fake()->unique()->randomNumber(3),
			"title" => fake()->sentence(2),
			"prix" => fake()->randomFloat(2, 1, 10000),
			"description"	=> fake()->sentence(15)
		];
	}
}
